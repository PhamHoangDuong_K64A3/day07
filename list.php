<?php
$faculties = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");

?>


<html>

<head>
    <meta charset="UTF-8">
    <title>day07 - list</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="list-wrapper">
        <form method="post" class="search-wrapper">
            <div class="select-wrapper">
                <label for="faculty" class="search-label">Khoa</label>
                <span class="select-arrow-2"></span>
                <select name="faculty" id="faculty" class="search-select">
                    <?php foreach ($faculties as $key => $value) : ?>
                        <option value=<?= $key; ?>><?= $value; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div>
                <label for="keyword" class="search-label">Từ khoá</label>
                <input type="text" name="keyword" id="keyword" class="search-input">
            </div>
            <input type="submit" value="Tìm kiếm" name="search" class="search-btn" />
        </form>

        <div>
            <table class="list-student" align="left">
                <tr>
                    <td colspan="3">Số sinh viên tìm thấy: XXX</td>
                    <td>
                        <a href="register.php" class="add-link">
                            <button class="add-btn">Thêm</button>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%;">No</td>
                    <td style="width: 30%;">Tên sinh viên</td>
                    <td style="width: 40%;">Khoa</td>
                    <td style="width: 30%;">Action</td>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Nguyễn Văn A</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <button class="action-btn">Xóa</button>
                        <button class="action-btn">Sửa</button>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Trần Thị B</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <button class="action-btn">Xóa</button>
                        <button class="action-btn">Sửa</button>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Nguyễn Hoàng C</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <button class="action-btn">Xóa</button>
                        <button class="action-btn">Sửa</button>
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Đinh Quang D</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <button class="action-btn">Xóa</button>
                        <button class="action-btn">Sửa</button>
                    </td>
                </tr>
            </table>
        </div>

    </div>

</body>

</html>